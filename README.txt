sudo apt-get install apache2 mysql-client mysql-server

sudo apt-get install libapache2-mod-wsgi

sudo a2enmod wsgi

cd /var/www/

mkdir beaconInventoryTracker

*copy project into that folder*

apt-get install python-pip

pip install Flask

nano /etc/apache2/sites-available/beaconInventoryTracker.conf

"""
    <VirtualHost *:80>
                    ServerName yourdomain.com
                    ServerAdmin youemail@email.com
                    WSGIScriptAlias / /var/www/beaconInventoryTracker/beaconInventoryTracker.wsgi
                    <Directory /var/www/beaconInventoryTracker/beaconInventoryTracker/>
                            Order allow,deny
                            Allow from all
                    </Directory>
                    Alias /static /var/www/beaconInventoryTracker/beaconInventoryTracker/static
                    <Directory /var/www/beaconInventoryTracker/beaconInventoryTracker/static/>
                            Order allow,deny
                            Allow from all
                    </Directory>
                    ErrorLog ${APACHE_LOG_DIR}/error.log
                    LogLevel warn
                    CustomLog ${APACHE_LOG_DIR}/access.log combined
    </VirtualHost>
"""

*Change the domain name above to the public IP adress of the computer*

sudo a2ensite beaconInventoryTracker

service apache2 reload

cd /var/www/beaconInventoryTracker

service apache2 restart