import sqlite3

conn = sqlite3.connect('skuFinder.db')
c = conn.cursor()

c.execute("CREATE TABLE IF NOT EXISTS items (SKU INT PRIMARY KEY, location CHAR(50), comment CHAR(140))")