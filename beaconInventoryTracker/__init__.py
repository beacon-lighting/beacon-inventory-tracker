from flask import Flask, render_template, request, url_for, redirect, flash
app = Flask(__name__)

from beaconInventoryTracker.functions import *

app.secret_key = 'dsd09sddds9d/,d97dfbf'

@app.errorhandler(404)
@app.errorhandler(500)
@app.errorhandler(401)
@app.errorhandler(400)
@app.errorhandler(403)
def page_not_found(e):
    return render_template('error.html', error = e)

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == "POST":
        sku = request.form['sku']
        return redirect(url_for("item", sku = sku))
    
    return render_template("index.html")
    
@app.route('/add_item', methods=['GET', 'POST'])
@app.route('/add_item/', methods=['GET', 'POST'])
def add_item():
    errors = []
    if request.method == "POST":
        sku      = request.form['sku']
        location = request.form['location']
        comment  = request.form['comment']
        errors = addItem(sku, location, comment)
        if not errors:
            flash('Item Added')
            return redirect(url_for("add_item"))
        
    return render_template("add_item.html",
                            errors = errors
    )
    
@app.route('/items')
@app.route('/items/')
def items():
    items = getAllItems()
    return render_template("items.html",
                            items = items
    )
    
@app.route('/items/<sku>', methods=['GET', 'POST'])
@app.route('/items/<sku>/', methods=['GET', 'POST'])
def item(sku=False):
    errors = []
    if request.method == "POST":
        if request.form['form'] == "edit_item":
            location = request.form['location']
            comment  = request.form['comment']
            errors = editItem(sku, location, comment)
            if not errors:
                flash('Item Updated')
                return redirect(url_for("item", sku = sku))
        elif request.form['form'] == "delete_item":
            deleteItem(sku)
            flash('Item Deleted')
            return redirect(url_for("items"))
        else:
            return redirect(url_for("item", sku = sku))

    if not doesItemExist(sku):
        return redirect(url_for("items"))
    location, comment = getItem(sku)
    return render_template("item.html",
                            sku      = sku,
                            location = location,
                            comment  = comment,
                            errors   = errors
    )