import sqlite3

def addItem(sku, location, comment):
    errors = []
    
    if len(sku) != 6:
        errors.append('SKU must be 6 digits long')
    if intCheck(sku) == False:
        errors.append('SKU must be a number')
    elif int(sku) < 0:
        errors.append('SKU must be positive')
    if location != "Shop Floor" and location != "Store Room" and location != "Pelmets" and location != "Staff Room":
        errors.append('Invalid location ' + location)
    if len(comment) > 140:
        errors.append('Comment is too long')
    
    if not errors:
        conn = sqlite3.connect('beaconInventoryTracker.db')
        c = conn.cursor()

        c.execute("INSERT INTO items (SKU, location, comment) VALUES (?, ?, ?)", [sku, location, comment])
        conn.commit()
        
        c.close()
        conn.close()
    
    return errors
    
def intCheck(s):
    try: 
        int(s)
        return True
    except:
        return False
    
def getItem(sku):
    conn = sqlite3.connect('beaconInventoryTracker.db')
    c = conn.cursor()

    c.execute("SELECT location, comment FROM items WHERE SKU = ?", [sku])
    itemData = c.fetchone()
    
    location = itemData[0]
    comment  = itemData[1]
    
    c.close()
    conn.close()
    
    return location, comment
    
def getAllItems():
    conn = sqlite3.connect('beaconInventoryTracker.db')
    c = conn.cursor()

    c.execute("SELECT * FROM items")
    allItems = c.fetchall()
    
    c.close()
    conn.close()
    
    return allItems
    
def doesItemExist(sku):
    conn = sqlite3.connect('beaconInventoryTracker.db')
    c = conn.cursor()

    c.execute("SELECT sku FROM items WHERE SKU = ?", [sku])
    doesItemExist = len(c.fetchall())
    
    c.close()
    conn.close()
    
    if doesItemExist > 0:
        return True
    else:
        return False
        
def deleteItem(sku):
    conn = sqlite3.connect('beaconInventoryTracker.db')
    c = conn.cursor()

    c.execute("DELETE FROM items WHERE SKU = ?", [sku])
    conn.commit()
    
    c.close()
    conn.close()
    
def editItem(sku, location, comment):
    errors = []
    
    if location != "Shop Floor" and location != "Store Room" and location != "Pelmets" and location != "Staff Room":
        errors.append('Invalid location ' + location)
    if len(comment) > 140:
        errors.append('Comment is too long')
    
    if not errors:
        conn = sqlite3.connect('beaconInventoryTracker.db')
        c = conn.cursor()

        c.execute("UPDATE items SET location = ?, comment = ? WHERE SKU = ?", [location, comment, sku])
        conn.commit()
        
        c.close()
        conn.close()
    
    return errors